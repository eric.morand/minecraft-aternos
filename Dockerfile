FROM alpine

RUN apk add openjdk11

RUN java -version

VOLUME /opt/aternos/world

WORKDIR /opt/aternos

RUN wget https://cdn.getbukkit.org/spigot/spigot-1.16.5.jar -O server.jar
RUN echo "eula=true" > eula.txt

ADD server.properties ./

ENTRYPOINT ["java", "-DIReallyKnowWhatIAmDoingISwear", "-Xms4G", "-Xmx4G", "-jar", "server.jar"]